import { RouteProp } from "@react-navigation/core"
import { StackNavigationProp } from "@react-navigation/stack"
import AuthorList from "./src/components/AuthorList"

export type Author = {
    id: string,
    name: string,
    age: string
}

export type Author2 = {
    id: string,
    name: string,
    age: string,
    asc: boolean,
    page: number,
    size: number,
    onRemoved: any,
    entries: any,
    onLoadMore: any
}

export type Anime = {
    id?: string,
    title?: string,
    description?: string,
    opSong?: string,
    edSong?: string
    author?: undefined
}

export type User = {
    id: string
    profile: { username: string, age: string }
}

export type AppTabsList = {
    Author: undefined;
    Anime: undefined;
}

export type AuthorStacks = {
    Author: undefined;
    CreateAuthor: undefined;
    EditAuthor: { id: string, name: string, age: string }
}


export type AnimeStacks = {
    Anime: undefined;
    CreateAnime: undefined;
}

export type AuthParamList = {
    Login: undefined;
    Register: undefined
}

export type AfterLoginList = {
    Home: undefined
    Profile: undefined
    Setting: undefined
    AboutUs: undefined
    Logout: undefined
}




export type HomeNavProps<T extends keyof AppTabsList> = {
    navigation: StackNavigationProp<AppTabsList, T>;
    route: RouteProp<AppTabsList, T>;
}

export type AuthorNavProps<T extends keyof AuthorStacks> = {
    navigation: StackNavigationProp<AuthorStacks, T>;
    route: RouteProp<AuthorStacks, T>;
}


export type AuthNavProps<T extends keyof AuthParamList> = {
    navigation: StackNavigationProp<AuthParamList, T>;
    route: RouteProp<AuthParamList, T>;
}

export type AfterLoginProps<T extends keyof AfterLoginList> = {
    navigation: StackNavigationProp<AfterLoginList, T>;
    route: RouteProp<AfterLoginList, T>;
}
