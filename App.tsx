import React from 'react';
import { ApolloProvider } from '@apollo/client';
import { client } from './src/client/client';
import Routes from './src/Routes';



function App() {
  return (
    <Routes />
  );
}




export default function AppWired() {
  return (
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  )
}


