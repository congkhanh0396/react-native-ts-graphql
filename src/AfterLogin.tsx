import React from 'react';
import { TouchableOpacity } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { AntDesign, Ionicons, EvilIcons } from '@expo/vector-icons';
import AnimeScreen from './screens/Anime/AnimeScreen'
import AuthorScreen from './screens/Author/AuthorScreen';
import { AfterLoginList, AnimeStacks, AppTabsList, AuthorNavProps, AuthorStacks } from '../types';
import { createStackNavigator } from '@react-navigation/stack';
import EditAuthorScreen from './screens/Author/EditAuthorScreen';
import CreateAuthorScreen from './screens/Author/CreateAuthorScreen';
import ProfileScreen from './screens/User/ProfileScreen';
import UpdateProfile from './screens/User/UpdateProfileScreen';
import UpdateProfileScreen from './screens/User/UpdateProfileScreen';
import Logout from './screens/Auth/Logout';
import DrawerContent from './DrawerContent';
import Settings from './screens/User/Settings';
import AboutUs from './screens/User/AboutUs';

interface AppTabsProps { }
interface AuthorStackProps { }
interface AnimeStackProps { }
interface AfterLoginProps { }

const AuthorsStack = createStackNavigator<AuthorStacks>();
const AnimesStack = createStackNavigator<AnimeStacks>();
const Tab = createBottomTabNavigator<AppTabsList>();
const Drawer = createDrawerNavigator<AfterLoginList>();


const AuthorStack: React.FC<AuthorStackProps> = () => (
    <AuthorsStack.Navigator>
        <AuthorsStack.Screen options={({ navigation, route }: AuthorNavProps<"Author">) => ({
            headerRight: () => (
                <TouchableOpacity onPress={() => { navigation.navigate("CreateAuthor") }}>
                    <Ionicons style={{ color: 'gray' }} name="add-circle-outline" size={25} />
                </TouchableOpacity>
            )
        })} name="Author" component={AuthorScreen} />
        <AuthorsStack.Screen name="CreateAuthor" component={CreateAuthorScreen} />
        <AuthorsStack.Screen name="EditAuthor" component={EditAuthorScreen} />
    </AuthorsStack.Navigator>
)

const AnimeStack: React.FC<AnimeStackProps> = ({ }) => (
    <AnimesStack.Navigator>
        <AnimesStack.Screen name="Anime" component={AnimeScreen} />
    </AnimesStack.Navigator>
)

const TabStack: React.FC<AppTabsList> = ({ }) => {
    return <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Author') {
                    iconName = "home";
                    return <AntDesign name={"user"} size={size} color={color} />;
                } else if (route.name === 'Anime') {
                    iconName = "search";
                    return <EvilIcons name={"heart"} size={size} color={color} />;
                }
                return <Ionicons name={iconName} size={size} color={color} />;
            }
        })}
        tabBarOptions={{
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray'
        }}
    >
        <Tab.Screen name="Author" component={AuthorStack} />
        <Tab.Screen name="Anime" component={AnimeStack} />
    </Tab.Navigator>;
}


const AfterLogin: React.FC<AppTabsProps> = ({ }) => {
    return (
        <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />} >
            <Drawer.Screen name="Home" component={TabStack} />
            <Drawer.Screen name="Profile" component={ProfileScreen}/>
            <Drawer.Screen name="Setting" component={Settings} />
            <Drawer.Screen name="AboutUs" component={AboutUs} />
            <Drawer.Screen name="Logout" component={Logout} />
        </Drawer.Navigator>

    )
}

export default AfterLogin;