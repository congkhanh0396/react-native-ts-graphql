import AsyncStorage from '@react-native-async-storage/async-storage';

const AUTH_TOKEN_KEY = 'MY_JWT_TOKEN';

export const getAuthToken = async (): Promise<string | null> => await AsyncStorage.getItem(AUTH_TOKEN_KEY);

export const signIn = (newToken: string) => AsyncStorage.setItem(AUTH_TOKEN_KEY, newToken);

export const logout = () => AsyncStorage.removeItem(AUTH_TOKEN_KEY);