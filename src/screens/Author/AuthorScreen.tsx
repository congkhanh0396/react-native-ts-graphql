import React, { useState } from 'react';
import AuthorList from '../../components/AuthorList';
import { View, Text, ActivityIndicator, TouchableOpacity, ScrollView } from 'react-native';
import { useQuery } from '@apollo/client';
import { Author, AuthorNavProps } from '../../../types';
import { Center } from '../../Center';
import { FIND_ALL_AUTHORS_SORT } from '../../client/query';
import { DataTable } from 'react-native-paper';

interface AuthorData { findAllAuthorsBySort: Author[], authorCount: number };

interface PageSize {
    ascending: boolean
}


const AuthorScreen = ({ navigation, route }: AuthorNavProps<"Author">) => {

    const [pageSize, setPageSize] = useState(8); // size item in 1 page

    const [page, setPage] = useState(0); // defaut page with 0

    const [field, setField] = useState('ID'); // sort by ID field

    const [ascending, setAscendingt] = useState(true); // sort order with ASC

    const { data, loading, error, refetch, fetchMore } = useQuery<AuthorData>(FIND_ALL_AUTHORS_SORT,  // fetchMore here
        {
            fetchPolicy: 'network-only',
            variables: {
                page: page,
                size: pageSize,
                field: field,
                order: ascending ? 'ASC' : 'DESC'
            }
        }); 

    console.log("=================================== tổng author", data?.authorCount);



    var authorCount = data?.authorCount; 


    const handleOnRemoved = () => { refetch(); } // define function HandleOnRemove



    console.log("show data please ---------------------------------", authorCount);


    const totalPage = authorCount ? Math.ceil(authorCount / pageSize) : 0;  // show total page
    console.log('------------------- total page is --------------', totalPage);


    if (!data && loading) return (<Center><ActivityIndicator size="large" color="tomato" /></Center>)

    if (error) return (<View><Text>Error ... :(</Text></View>)

    return (
        <ScrollView showsVerticalScrollIndicator={false}  // Hide scrollbar
            showsHorizontalScrollIndicator={false}
        >
            <DataTable>
                <DataTable.Header style={{ backgroundColor: 'white' }}>
                    <TouchableOpacity onPress={() => setAscendingt(!ascending)}>
                        <DataTable.Title sortDirection={ascending ? 'ascending' : 'descending'}>ID</DataTable.Title>
                    </TouchableOpacity>
                    <DataTable.Title numeric>Name</DataTable.Title>
                    <DataTable.Title numeric>Age</DataTable.Title>
                    <DataTable.Title numeric>Delete</DataTable.Title>
                </DataTable.Header>
                {data && data.findAllAuthorsBySort.map((t: Author) => (
                    <TouchableOpacity key={t.id} onLongPress={() => navigation.navigate("EditAuthor", { id: t.id, name: t.name, age: t.age })}>
                        <AuthorList
                            entries={data && data.findAllAuthorsBySort || []}
                            onLoadMore={() => fetchMore({ variables: { page: page, size: pageSize, field, order: ascending ? 'ASC' : 'DESC' } })}
                            id={t.id}
                            name={t.name}
                            age={t.age}
                            page={page}
                            size={pageSize}
                            asc={ascending}
                            onRemoved={handleOnRemoved} />
                    </TouchableOpacity>

                ))}
                <DataTable.Pagination
                    style={{ backgroundColor: 'white' }}
                    page={page}
                    numberOfItemsPerPage={pageSize}
                    numberOfPages={authorCount ? Math.ceil(authorCount / pageSize) : 0}
                    //
                    onPageChange={(page) => setPage(page)}  
                    
                    showFastPaginationControls
                    label={`${page + 1} of ${totalPage}`}
                />
            </DataTable>
        </ScrollView>

    )

}

export default AuthorScreen;