import React, { useState } from 'react';
import { Card, Input, Button } from 'react-native-elements';
import { Ionicons } from '@expo/vector-icons';
import { useMutation } from '@apollo/client';
import { CREATE_AUTHOR } from '../../client/mutation';
import { AuthorNavProps } from '../../../types';
import { Alert } from 'react-native';

type Author = {
    name: string,
    age: string
}

const CreateAuthorScreen = ({ navigation, route }: AuthorNavProps<"CreateAuthor">) => {

    const [author, setAuthor] = useState<Author>({ name: '', age: '' });


    const [insertAuthor, { data, error }] = useMutation(CREATE_AUTHOR, {
        update: (cache, { data }) => {
            cache.modify({
                fields: {
                    findAllAuthorsBySort(exisiting = []) {
                        if (!data) return exisiting;
                        return [...exisiting, data]
                    }
                }
            })
        }
    });


    const addAuthor = () => {
        const newData = { name: author.name, age: author.age };
        insertAuthor({
            variables: newData
        });
        // console.log("---------------------",data);

        Alert.alert("Created", "Author: " + author.name + " Age: " + author.age);
        return { data, error };
    }

    return (
        <Card>
            <Card.Title>Add Author</Card.Title>
            <Card.Divider />
            <Input
                placeholder='Author here...'
                inputContainerStyle={{ borderWidth: 0 }}
                leftIcon={<Ionicons name='person-add-outline' size={18} color='gray' style={{ padding: 5 }} />}
                onChangeText={(name = '') => setAuthor({ ...author, name })}
            />
            <Input
                placeholder='Age here...'
                inputContainerStyle={{ borderWidth: 0 }}
                leftIcon={<Ionicons name='heart-outline' size={18} color='gray' style={{ padding: 5 }} />}
                onChangeText={(age = '') => setAuthor({ ...author, age: age.replace(/[^0-9]/g, '') })}
                keyboardType='numeric'
            />
            <Button
                title="add"
                buttonStyle={{ borderRadius: 5, backgroundColor: 'tomato' }}
                onPress={() => addAuthor()}
                disabled={author.age === '' || author.age === '0' || author.name === ''}
            />
        </Card>

    )
}

export default CreateAuthorScreen;