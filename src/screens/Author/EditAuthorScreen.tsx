import React, { useState } from 'react';
import { Card, Input, Button } from 'react-native-elements';
import { Ionicons } from '@expo/vector-icons';
import { useMutation } from '@apollo/client';
import { UPDATE_AUTHOR } from '../../client/mutation';
import { Author, AuthorNavProps } from '../../../types';
import { ActivityIndicator, Alert } from 'react-native';
import { Center } from '../../Center';


interface AuthorData { Author: Author };

const EditAuthorScreen = ({ route }: AuthorNavProps<"EditAuthor">) => {

    const { id, name, age } = route.params;

    // console.log("this is my id", id, name, age);

    const [Name, setName] = useState(name || '');

    const [Age, setAge] = useState(age || '');


    const [updateAuthor, {data, loading, error }] = useMutation<AuthorData>(UPDATE_AUTHOR, {
        update: (cache, { data }) => {
            cache.modify({
                fields: {
                    findAllAuthorsBySort(exisiting = []) {
                        if (!data) return exisiting;
                        return [...exisiting, data]
                    }
                }
            })
        }
    });
    //   console.log("----------------EDIT------------------------",data);

    if (loading) return (<Center><ActivityIndicator size="large" color="tomato" /></Center>)

    if (error) { console.log(error); }

    const handleUpdateAuthor = () => {
        updateAuthor({
            variables: { id: `${id}`, name: Name, age: Age }
        });
        Alert.alert("Updated", "Author: " + Name + " Age: " + Age);


        return { ...data, error };

    }

    return (
        <Card>
            <Card.Title>Edit Author</Card.Title>
            <Card.Divider />
            <Input
                placeholder='Author here...'
                value={Name}
                inputContainerStyle={{ borderWidth: 0 }}
                leftIcon={<Ionicons name='person-add-outline' size={18} color='gray' style={{ padding: 5 }} />}
                onChangeText={(Name) => setName(Name)}
            />
            <Input
                placeholder='Age here...'
                value={Age}
                inputContainerStyle={{ borderWidth: 0 }}
                leftIcon={<Ionicons name='heart-outline' size={18} color='gray' style={{ padding: 5 }} />}
                onChangeText={(Age) => setAge(Age.replace(/[^0-9]/g, ''))}
                keyboardType='numeric'
            />
            <Button
                title="Update"
                buttonStyle={{ borderRadius: 5, backgroundColor: 'tomato' }}
                disabled={Age === ''|| Age === '0' || Name === ''}
                onPress={() => handleUpdateAuthor()}
            />
        </Card>

    )
}

export default EditAuthorScreen;