import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import { useQuery } from '@apollo/client';

import { Anime } from '../../../types';
import AnimeList from '../../components/AnimeList';
import { Center } from '../../Center';
import { FIND_ALL_ANIMES } from '../../client/query';

interface AnimeData { findAllAnimes: Anime[] };

const AnimeScreen = () => {

    const { data, loading, error } = useQuery<AnimeData>(FIND_ALL_ANIMES);

    if (loading) return (<Center><ActivityIndicator size="large" color="tomato" /></Center>)

    if (error) return (<View><Text>Error ... :(</Text></View>)

    return (
        <View>
            {data && data.findAllAnimes.map((a: Anime) => {
                // console.log("hello", data.findAllAnimes);
                return (
                    <AnimeList key={a.id} title={a.title} description={a.description} opSong={a.opSong} edSong={a.edSong} />
                )
            })}
        </View>
    )
}





export default AnimeScreen;