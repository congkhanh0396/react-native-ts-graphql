import React from 'react'
import { View, Text, Button } from 'react-native'
import { Center } from '../../Center'
import { logout } from '../../storage/AuthenticationStorage'

export default function Logout() {
    return (
        <Center>
            <View>
                <Text>Logout</Text>
                <Button title='logout' onPress={() => logout()}>Logout</Button>
            </View>
        </Center>
    )
}
