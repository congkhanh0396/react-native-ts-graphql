import React, { useState } from 'react';
import { Text, StyleSheet, View, TouchableOpacity, Alert } from 'react-native';
import { AuthNavProps } from '../../../types';
import { Input, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import { CREATE_USER } from '../../client/mutation';
import { useMutation } from '@apollo/client';

type User = {
    username: string,
    email: string,
    password: string
}

const register = ({ navigation, route }: AuthNavProps<"Register">) => {

    const [user, setUser] = useState<User>({username: '', email:  '', password: ''});

    const [showPassword, setShowPassword] = useState(false);

    const [addUser, { data, error }] = useMutation(CREATE_USER);

    function addUserHandle(){
        const newData = { username: user.username, email: user.email, password: user.password };
        addUser({
            variables: newData
        });
        Alert.alert("Register Successfully",  user.username);
        return { data, error };
    }


    return (
        <View style={styles.container}>
            <View style={styles.title_wrapper}>
                <Text style={styles.title}>Create New Account</Text>
            </View>
            <View style={styles.form}>
                <Input
                    inputContainerStyle={styles.input}
                    placeholder="Usename"
                    leftIcon={
                        <Icon
                            name='person'
                            size={20}
                            color='#808080'
                            style={{ padding: 10 }}
                        />
                    }
                    onChangeText={(username = '') => setUser({ ...user, username })}
                />
                <Input
                    inputContainerStyle={styles.input}
                    placeholder="Email"
                    leftIcon={
                        <Icon
                            name='mail'
                            size={20}
                            color='#a8a8a8'
                            style={{ padding: 10 }}
                        />
                    }
                    onChangeText={(email = '') => setUser({ ...user, email })}
                />
                <Input
                    inputContainerStyle={styles.input}
                    placeholder="Password"
                    maxLength={16}
                    secureTextEntry={showPassword ? false : true}
                    leftIcon={
                        <Icon
                            name='lock-closed'
                            size={20}
                            color='#a8a8a8'
                            style={{ padding: 10 }}
                        />
                    }
                    rightIcon={
                            <Icon
                                name={showPassword ? 'eye-outline' : 'eye-off-outline'}
                                size={20}
                                color='#a8a8a8'
                                style={{ padding: 10 }}
                                onPress={() => setShowPassword(!showPassword)}
                            />
                    }
                    onChangeText={(password = '') => setUser({ ...user, password })}
                />

                <Button buttonStyle={{ width: 150, alignSelf: 'center', borderRadius: 50, backgroundColor: '#fa466d', marginTop: 25 }} title="Continue" 
                  onPress={() => addUserHandle()} 
                  disabled={user.username === '' || user.email === '' || user.password === ''}
                  />

            </View>
            <View style={styles.footer}>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    <Text style={{ color: '#adaeaf' }}>Already has an account?</Text>
                </TouchableOpacity>
            </View>
        </View>

    )
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        height: "100%",
        width: "100%"
    },


    title_wrapper: {
        marginVertical: 30,
        alignSelf: 'center',
    },

    title: {
        color: '#fa466d',
        fontSize: 35,
    },

    btn_back: {
        position: 'absolute',
        bottom: 0,
        width: '100%',

    },
    form: {
        padding: 8
    },

    input: {
        borderWidth: 1,
        borderRadius: 50,
        backgroundColor: 'white',
        borderColor: '#a8a8a8',
        marginTop: 15
    },
    footer: {
        flex: 1,
        flexDirection: 'row',
        padding: 5,
        marginTop: '10%',
        justifyContent: 'space-around'
    }
})

export default register;