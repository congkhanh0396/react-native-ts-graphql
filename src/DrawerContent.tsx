import React from 'react'
import { StyleSheet, View } from 'react-native';
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import { Avatar, Title, Caption, Paragraph, Drawer } from 'react-native-paper';
import { Ionicons } from '@expo/vector-icons';
import { useQuery } from '@apollo/client';
import { User } from '../types';
import { GET_CURRENT_USER } from './client/query';

interface UserData { getCurrentUser: User };


export default function DrawerContent(props: any) {

    const { data, loading, error } = useQuery<UserData>(GET_CURRENT_USER,
        {
            fetchPolicy: 'cache-and-network'
        }

    );


    return (
        <View style={{ flex: 1 }}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <View style={styles.userInfoSelection}>
                        <View style={{ flexDirection: 'row', marginTop: 15 }}>
                            <Avatar.Image
                                source={{
                                    uri: 'https://i.pinimg.com/1200x/a6/11/11/a6111193daab4f6e85f0cc439e175f2e.jpg'
                                }}
                                size={50}
                            />
                            <View style={{ marginLeft: 15, flexDirection: 'column' }}>
                                <Title style={styles.title}>{data && data?.getCurrentUser.profile.username}</Title>
                                <Caption style={styles.caption}>@..{data && data?.getCurrentUser.profile.age}.aztq</Caption>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>80</Paragraph>
                                <Caption style={styles.caption}>Following</Caption>
                            </View>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>100</Paragraph>
                                <Caption style={styles.caption}>Followers</Caption>
                            </View>
                        </View>
                    </View>
                </View>
                <Drawer.Section style={styles.drawerSection}>
                    <DrawerItem
                        icon={({ color, size }) => (
                            <Ionicons
                                name="home-outline"
                                color={color}
                                size={size}
                            />
                        )}

                        label="Home"
                        onPress={() => { props.navigation.navigate('Home') }}
                    />
                    <DrawerItem
                        icon={({ color, size }) => (
                            <Ionicons
                                name="person-circle-outline"
                                color={color}
                                size={size}
                            />
                        )}
                        label="Profile"
                        onPress={() => { props.navigation.navigate('Profile') }}
                    />
                    <DrawerItem
                        icon={({ color, size }) => (
                            <Ionicons
                                name="settings-outline"
                                color={color}
                                size={size}
                            />
                        )}
                        label="Settings"
                        onPress={() => { props.navigation.navigate('Setting') }}
                    />
                    <DrawerItem
                        icon={({ color, size }) => (
                            <Ionicons
                                name="information-circle-outline"
                                color={color}
                                size={size}
                            />
                        )}
                        label="About Us"
                        onPress={() => { props.navigation.navigate('AboutUs') }}
                    />
                </Drawer.Section>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.bottomDrawerSection}>
                <DrawerItem
                    label="Sign out"
                    icon={({ focused: boolean, size: number, color: string }) => {
                        return <Ionicons name='log-out-outline' size={20} color="gray" />;
                    }}
                    onPress={() => { props.navigation.navigate('Logout') }}
                />
            </Drawer.Section>
        </View>
    )
}

const styles = StyleSheet.create({
    drawerContent: {
        flex: 1
    },
    userInfoSelection: {
        paddingLeft: 20
    },
    title: {
        fontSize: 16,
        marginTop: 3,
        fontWeight: 'bold',
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
    },
    drawerSection: {
        marginTop: 15,
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: 3,
    },

});
