import { ApolloClient, InMemoryCache, gql, ApolloProvider, useQuery, createHttpLink } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { getAuthToken } from '../storage/AuthenticationStorage';

const httpLink = createHttpLink({
  uri: 'https://graphql-by-kan.herokuapp.com/apis/graphql',
});

const authLink = setContext(async (_, {cache, headers }) => {

  const jwt = await getAuthToken();
  
  return { headers: { ...headers, authorization: jwt ? `Bearer ${jwt}` : '', } }
});


export const client = new ApolloClient({
  link: authLink.concat(httpLink),
  credentials: 'include', 

  cache: new InMemoryCache({
    typePolicies: {
      Query: {
        fields: {
          findAllAuthorsBySort: {
            merge(existing = [], incoming: any[], { variables }) {
              const result = existing ? existing.slice(0) : [];
              if (variables) {
                const { page = 0, size = 0 } = variables;
                const offset = page * size;
                for (let i = 0; i < incoming.length; ++i) {
                  result[offset + i] = incoming[i];
                }
                return result;
              }
              else {
                result.push.apply(result, incoming);
              }
            }
          },
        },
      },
    },
  }),
});