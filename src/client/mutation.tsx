import { gql } from '@apollo/client';

export const CREATE_AUTHOR = gql`
    mutation CreateAuthor($name: String!, $age: String!){
        createAuthor(
            name: $name
            age: $age){
                name
                age
            }
    }
`;

export const UPDATE_AUTHOR = gql`
mutation UpdateAuthor($id: ID!, $name: String!, $age: String!){
    updateAuthor(
        id: $id
        name: $name
        age: $age){
            name
            age
        }
}
`;

export const DELETE_AUTHOR = gql`
mutation DeteleAuthor($id: ID!){
    deleteAuthor(
        id: $id
    )
}
`;




export const CREATE_USER = gql`
mutation CreateUser($username: String!, $email: String!, $password: String!)
{
    createUser(
        createUser:{
                 username: $username
                 email: $email
                 password: $password
            }, 
        createProfile:{}
        )
        {
            id
            roles
        }
}
`;

export const LOGIN = gql`
  mutation Login($email: String!, $password: String!){
      login(
          email: $email
          password: $password){
            id
            token
            roles
          }
  }
`;
