import { gql } from '@apollo/client';

export const FIND_ALL_AUTHORS = gql`
query{
  findAllAuthors{
    id
    name
    age
  }
}
`;

export const AUTHOR_COUNT = gql`
query{
  authorCount
}
`;


export const FIND_ALL_AUTHORS_SORT_ID = gql`
query{
  findAllAuthorsBySort(page: 0, size: 10, sort: {field: ID, order: ASC}){
    id
    name
    age
  }
}
`;

export const FIND_ALL_AUTHORS_SORT = gql`

query($page: Int!, $size: Int!, $field: AuthorSortField, $order: Order){ 
  findAllAuthorsBySort(page: $page, size:  $size, sort: {field: $field, order:  $order}){
    id
    name
    age
  },
  authorCount
}
`;


export const FIND_ALL_ANIMES = gql`
query{
  findAllAnimes{
    id
    title
    description
    opSong
    edSong
    author{
      id
      name
    }
  }
}
`;


export const AUTHOR = gql`
    query{
        author(id: String){
            author(id: id){
            id
            name
            age
        }
        }
    }
`;

export const GET_CURRENT_USER = gql`
    query{
      getCurrentUser{
        id
        roles
        profile{
          username
          age
        }
      }
    }
`

