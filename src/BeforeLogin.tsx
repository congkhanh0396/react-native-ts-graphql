import { createStackNavigator, HeaderTitle } from '@react-navigation/stack';
import React from 'react';
import { AuthParamList } from '../types';
import LoginScreen from './screens/Auth/LoginScreen';
import RegisterScreen from './screens/Auth/RegisterScreen';

const Stack = createStackNavigator<AuthParamList>();

interface AuthStackProps {

}

export const BeforeLogin: React.FC<AuthStackProps> = ({ }) => {
    return (
        < Stack.Navigator initialRouteName="Login" >
            <Stack.Screen options={{ headerTitle: "Sign In", headerTintColor: 'white', headerStyle: { backgroundColor: '#fa466d' } }} name="Login" component={LoginScreen} />
            <Stack.Screen options={{ headerTitle: "Sign Up", headerShown: true, headerTintColor: 'white', headerStyle: { backgroundColor: '#fa466d' }}} name="Register" component={RegisterScreen} />
        </Stack.Navigator>
    )
}