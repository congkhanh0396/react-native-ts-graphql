import { useMutation } from '@apollo/client';
import React, { useState } from 'react';
import { ActivityIndicator, Alert, TouchableOpacity } from 'react-native';
import { ListItem, Icon } from 'react-native-elements'
import { DataTable } from 'react-native-paper';
import { Author2 } from '../../types';
import { Center } from '../Center';
import { DELETE_AUTHOR } from '../client/mutation';
import { FIND_ALL_AUTHORS_SORT, FIND_ALL_AUTHORS_SORT_ID, FIND_ALL_AUTHORS } from '../client/query';


const AuthorList = (props: Author2) => {

    const [deleteAuthor, { data, loading, error }] = useMutation(DELETE_AUTHOR, {
      
            // onCompleted:  () => {props.refetch()}  方法１
            onCompleted: (id: string) => { props.onRemoved(id)}　//方法２
    });

    if (loading) return (<Center><ActivityIndicator size="large" color="tomato" /></Center>);

    if (error) { console.log(error); }

    const handleDeleteAuthor = (id: string) => {
        Alert.alert(
            'Delete',
            'Are You Sure? You wont`t be revert this!',
            [
                { text: 'NO', onPress: () => console.log("Cancel Pressed"), style: 'cancel' },
                {
                    text: 'YES', onPress: () => deleteAuthor({
                        variables: { id: `${id}` }
                    })
                },
            ]
        );
        return { data, error };

    }

    return (

        // <ListItem bottomDivider>
        //     <ListItem.Content key={props.id}>
        //         <ListItem.Title>{props.id} -- Author: {props.name} </ListItem.Title>
        //         <ListItem.Subtitle>Age: {props.age}</ListItem.Subtitle>
        //     </ListItem.Content>
        //     <TouchableOpacity onPress={() => handleDeleteAuthor(props.id)}><Icon name='trash-outline' type='ionicon' color='#517fa4' size={18} /></TouchableOpacity>
        // </ListItem>
        <DataTable.Row style={{ backgroundColor: 'white' }} key={props.id}>
            <DataTable.Cell>{props.id}</DataTable.Cell>
            <DataTable.Cell numeric>{props.name}</DataTable.Cell>
            <DataTable.Cell numeric>{props.age} </DataTable.Cell>
            <DataTable.Cell numeric><TouchableOpacity onPress={() => handleDeleteAuthor(props.id)}><Icon name='trash-outline' type='ionicon' color='#517fa4' size={18} /></TouchableOpacity></DataTable.Cell>
        </DataTable.Row>
    )
}

export default AuthorList;