import React from 'react';
import { Alert, StyleSheet, TouchableOpacity } from 'react-native';
import { ListItem, Icon } from 'react-native-elements'
import { Anime, HomeNavProps } from '../../types';

const AnimeList = (props: Anime) => {
    return (
        <TouchableOpacity onLongPress={()=>Alert.alert("Long press")}>
            <ListItem bottomDivider>
                <ListItem.Content>
                    <ListItem.Title>Title: {props.title}  </ListItem.Title>
                    <ListItem.Subtitle>{props.description} </ListItem.Subtitle>
                    <ListItem.Subtitle>Opening: {props.opSong} </ListItem.Subtitle>
                    <ListItem.Subtitle >Ending: {props.edSong} </ListItem.Subtitle>
                </ListItem.Content>
                <ListItem.Chevron color="tomato" />
            </ListItem>
        </TouchableOpacity>
    )
}

export default AnimeList;