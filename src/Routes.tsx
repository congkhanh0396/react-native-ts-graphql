import React from 'react';
import AfterLogin from './AfterLogin';
import { NavigationContainer } from '@react-navigation/native';
import { BeforeLogin } from './BeforeLogin';
import useAuthenticated from './isAuthenticated';

interface RoutesProps {

}

const Routes: React.FC<RoutesProps> = ({ }) => {

    const isAuthenticated = useAuthenticated();

    return (

        <NavigationContainer>
            {isAuthenticated ? (<AfterLogin />) : (<BeforeLogin />)}
        </NavigationContainer>
    )
}

export default Routes;