import React from "react";
import { getAuthToken } from "./storage/AuthenticationStorage";


export default function useAuthenticated() {
    const [isAuthenticated, setAuthenticated] = React.useState(false);
    React.useEffect(() => {
        async function loadAuthenticated() {
            const jwt = await getAuthToken();
            if (!jwt) setAuthenticated(false)
            else setAuthenticated(true)
        }
        loadAuthenticated();
    }, []);
    return isAuthenticated;
}